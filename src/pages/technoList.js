import TechnoItem from "../components/TechnoItems";

export default function TechnoList(props) {
  // Object disctructuring
  const { technos, handleDeleteTechno } = props;
  return (
    <div className="techno-list">
      <h1>Liste des Technos</h1>
      <div>
        {technos.map((techno) => (
          <TechnoItem techno={techno} key={techno.technoid} handleDeleteTechno={handleDeleteTechno} />
        ))}
      </div>
    </div>
  );
}
