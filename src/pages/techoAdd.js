import { useState } from "react";

// Récupères les props en passant props en paramètre (handleAddTechno) ici
export default function TechnoAdd(props) {
  const [techno, setTechno] = useState({
    technoName: "",
    technoCategory: "",
    technoDescription: "",
  });

  const { handleAddTechno } = props;

  // handleSubmit utilise handleAddTechno
  function handleSubmit(evt) {
    // Pour eviter que la page se rafraichisse
    evt.preventDefault();
    // Récupère les données du formulaire au submit
    handleAddTechno(techno);
    // Reset le formulaire apres le submit
    setTechno({
      technoName: "",
      technoCategory: "",
      technoDescription: "",
    });
  }

  function handleChange(evt) {
    // Desctructuring de l'objet
    const { name, value } = evt.target;
    // Immutabilité, cloné techno pour le modifier au lieu de modifier directement l'objet
    // [name]: value recupère la valeur sélectionné, name categorie ou description
    setTechno({ ...techno, [name]: value });
  }

  return (
    <div className="techno-add">
      <h1>Ajouter une techno</h1>
      <div>
        <form onSubmit={(evt) => handleSubmit(evt)}>
          <label htmlFor="technoname">Nom:</label>
          <br />
          <input
            type="text"
            name="technoName"
            id="technoname"
            value={techno.technoName}
            onChange={(evt) => handleChange(evt)}
          />
          <br />
          <label htmlFor="technocategory">Catégorie:</label>
          <select
            name="technoCategory"
            id="technocategory"
            value={techno.technoCategory}
            onChange={(evt) => handleChange(evt)}
          >
            <option value="">Sélectionner une catégorie</option>
            <option value="front">Front</option>
            <option value="back">Back</option>
            <option value="fullstack">Full Stack</option>
            <option value="other">Autre</option>
          </select>
          <br />
          <label htmlFor="technodescription">Description:</label>
          <br />
          <textarea
            name="technoDescription"
            id="technodescription"
            cols="30"
            rows="10"
            value={techno.technoDescription}
            onChange={(evt) => handleChange(evt)}
          ></textarea>
          <input type="submit" value="Ajouter une techno" className="btn" />
        </form>
      </div>
    </div>
  );
}
