import { useState, useEffect } from "react";
import { Routes, Route } from "react-router-dom";
import { v4 as uuidv4 } from "uuid";

import Home from "./pages/home";
import Menu from "./components/menu";
import "./css/app.css";
import TechnoAdd from "./pages/techoAdd";
import TechnoList from "./pages/technoList";
import { useLocalStorage } from "./hooks/useLocalStorage";

function App() {
  // Getter, Setter que l'on stocke dans un array vide
  // useState est une fonction qui démarre avec un état initiale en l'occurrence ici un array vide, peut être booleen, nbr, string etc
  // ici [{name: 'react', category: 'front', description: 'learn react'}, {}, {}...]
  // useState nous retourne donc une fonction nous retourne le tableau des technos et une fonction nous permettant de modifier ces technos
  const [technos, setTechnos] = useState([]);
  // Storage key
  const STORAGE_KEY = "technos";
  // useLocaleStorage, attends la clé et une valeur initiale fonctionne comme useState
  const [storedTechnos, setStoredTechnos] = useLocalStorage(STORAGE_KEY, []);

  // Au moment du premier chargement de App on recupère tout ce ce qu'il se trouve dans le localeStorage puis à l'aide du setter on récupère les technos
  useEffect(() => {
    setTechnos(storedTechnos);
  }, []);

  // useEffect réagit au changement d'etat de techno
  // Utiliser pour la persistance des données
  useEffect(() => {
    setStoredTechnos(technos);
  }, [technos]);

  // handleAddTechno gère la soummision du formulaire en récupérant les données du formulaire
  function handleAddTechno(techno) {
    console.log("handleAddTechno", techno);
    // Setter,  clone puis ajoute un nouvel objet
    // On clone l'objet techno pour ajouter un uuid
    setTechnos([...technos, { ...techno, technoid: uuidv4() }]);
  }

  function handleDeleteTechno(id) {
    // On prends array de toutes nos technos, pour chaques technos on veut garder toutes les technos sauf celle qui à l'id qui à été envoyé
    setTechnos(technos.filter((tech) => tech.technoid !== id));
  }

  // Envoi de la fonction du parent handleAddTechno vers son enfant TechnoAdd via une props

  return (
    <>
      <Menu />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route
          path="/add"
          element={<TechnoAdd handleAddTechno={handleAddTechno} />}
        />
        <Route
          path="/list"
          element={
            <TechnoList
              technos={technos}
              handleDeleteTechno={handleDeleteTechno}
            />
          }
        />
      </Routes>
    </>
  );
}

export default App;
