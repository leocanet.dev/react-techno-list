export default function TechnoItem(props) {
  const { techno, handleDeleteTechno } = props;
  return (
    // Unique key avec uuid
    <div key={techno.technoid} className="card">
      <h2>{techno.technoName}</h2>
      <h3>Categorie</h3>
      <p>{techno.technoCategory}</p>
      <h3>Description</h3>
      <p>{techno.technoDescription}</p>
      <div className="footer">
        <button className="btn-delete" onClick={() => handleDeleteTechno(techno.technoid)}>Supprimer</button>
      </div>
    </div>
  );
}
